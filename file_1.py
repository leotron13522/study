class Class_cont_men:
    def __init__(self, file_name, *args, **kwargs):
        print("create context manager")
        self.file_name = file_name
        self.args = args
        self.kwargs = kwargs
        self.file = None

    def __enter__(self):
        print("we open:" + self.file_name)
        self.file = open(self.file_name, *self.args, **self.kwargs)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("we close:" + self.file_name)
        self.file.close()

x = open("./test_f.py")
for j in x.readlines():
    print(x)
x.close()

with Class_cont_men("./test_f.py") as test_file:
    for j in test_file.readlines():
        print(j)