from flask import Flask
app = Flask(__name__)


@app.route('/calc/<int:first_numb>/+/<int:second_numb>')
def show_sum(first_numb, second_numb):
    return "your sum: %d" % (first_numb + second_numb)

@app.route('/calc/<int:first_numb>/-/<int:second_numb>')
def show_minus(first_numb, second_numb):
    return "your minus: %d" % (first_numb - second_numb)

@app.route('/calc/<int:first_numb>/*/<int:second_numb>')
def show_multi(first_numb, second_numb):
    return "your multi: %d" % (first_numb * second_numb)

@app.route('/calc/<int:first_numb>/div/<int:second_numb>')
def show_div(first_numb, second_numb):
    return "your div: %f" % (first_numb / second_numb)

if __name__ == '__main__':
    app.run(host="192.168.0.14")
