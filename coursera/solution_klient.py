import time
import socket


class Client:

    def connection(self, host, port, timeout):
        print(host, port)
        con = socket.socket()
        con.settimeout(timeout)
        con.connect((host, port))
        #con.send(b"trying to connect")
        #print(con, type(con))
        return con

    def __init__(self, host, port, timeout=None):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.connect = self.connection(self.host, self.port, self.timeout)
        #print(type(self.connect))

    def get(self, metrik_name):
        try:
            self.connect.send('get {}\n'.format(metrik_name).encode('utf-8'))
            ans = ""
            while True:
                rec = self.connect.recv(1024)
                #print(rec.decode('utf-8'))
                ans = ans + rec.decode('utf-8')
                #ans = ans.decode('utf-8')
                print("incoming message: ", ans)
                if ans[-2:] == '\n\n':
                    print("get catched")
                    returning_answer = self.making_dict(ans)
                    return returning_answer
                break
        except Exception:
            raise ClientError

    def put(self, metrik_name, metrik_value, timestamp=int(time.time())):
        try:
            print("trying to send")
            self.connect.send('put {} {} {}\n'.format(metrik_name, metrik_value, timestamp).encode('utf-8'))
            print(self.connect.recv(1024))
            print("success")
        except Exception:
            raise ClientError

    def close_connect(self):
        print("trying to close connection")
        self.connect.close()
        print("con close")

    def making_dict(self, ans):
        print("working with ans")
        ans_work = ans.split("\n")
        print(ans_work)
        ans_dict = {}
        for part in ans_work[1:]:
            if len(part) != 0:
                key, value, timestamp = part.split()
                print("need to include in dict: ", key, value, timestamp)
                if key not in ans_dict:
                    ans_dict[key] = [(int(timestamp), float(value))]
                else:
                    ans_dict[key] += [(int(timestamp), float(value))]
        return ans_dict


class ClientError(Exception):
    pass



client = Client("127.0.0.1", 10005)
client.put("test", 11, 22)
client.put("test", 33, 44)
client.put("test", 55, 66)
client.put("tost", 11, 22)
client.put("test", 89, 22)
print("final ans:", client.get("tist"))
print("final ans:", client.get("test"))
print("final ans:", client.get("*"))
client.get(" ")

client.close_connect()
