class FileReader:
    def __init__(self, file_path):
        self.file_path = file_path

    def read(self):
        try:
            with open(self.file_path) as f:
                text = f.read()
            return text
        except FileNotFoundError:
            return ''


if __name__ == "__main__":
    with open('some_file.txt', 'w') as file:
        file.write('some text')

    reader = FileReader('some_file.txt')
    text = reader.read()
    print(text)

    reader = FileReader('some.txt')
    text = reader.read()
    print(text)

