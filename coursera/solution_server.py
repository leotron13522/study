import socket
import threading
import time

storage = {}

def put_message(conn, message):
    print("message put")
    if len(message) == 3:
        key, value, timestamp = message[1], message[2], time.time()
    else:
        key, value, timestamp = message[1], message[2], message[3]
    conn.sendall(b"ok\n\n")
    if key not in storage:
        storage[key] = {timestamp: value}
    else:
        storage[key][timestamp] = value
    print("storage have: ", storage)

def get_message(conn, message):
    key = message[1]
    print("message get is: ", message[1])
    ans = []
    if key == "*":
        for keys in storage:
            print(keys)
            for values in storage[keys]:
                print(values)
                ans.append((keys, values, storage[keys][values]))
                #ans += "{} {} {}\n".format(keys, values, storage[keys][values])
    else:
        if key in storage:
            print("try to find: ", message[1])
            for keys in storage[key]:
                print(keys)
                ans.append((key, keys, storage[message[1]][keys]))
                #ans += "{} {} {}\n".format(key, keys, storage[message[1]][keys])
    ans.sort(key=lambda i: i[1])
    if ans == []:
        return_message = "ok\n\n"
    else:
        return_message = "ok\n" + '\n'.join(map(' '.join, ans)) + "\n\n"
    print("sending messsage:", return_message)
    conn.send(return_message.encode("utf8"))

def process_request(conn, adr):
    print("connected: ", adr)
    with conn:
        while True:
            data = conn.recv(1024)
            message = data.decode("utf8").split()
            if not data or not message:
                break
            print("incoming message is: ", message)
            data_start = message[0]
            if data_start == "put" and 3 <= len(message) <= 4:
                put_message(conn, message)
            elif data_start == "get" and len(message) == 2:
                get_message(conn, message)
            else:
                conn.sendall(b"error\nwrong command\n\n")


def run_server(host, port):
    with socket.socket() as sock:
        sock.bind((host, port))   # max port 65535
        sock.listen()
        print("connection created")
        while True:
            conn, addr = sock.accept()
            th = threading.Thread(target=process_request, args=(conn, addr))
            th.start()


run_server("127.0.0.1", 8888)

