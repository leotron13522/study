class Account2:

    def __init__(self, comission):
        self.comission = comission

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        self._amount = int(value * (1 - self.comission))

    @amount.getter
    def amount(self):
        return self._amount

vova = Account2(0.1)
vova.amount = 100
print(vova.amount)
