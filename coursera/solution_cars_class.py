import os
import csv
import re


class CarBase:
    def __init__(self, car_type, brand, photo_file_name, carrying):
        self.car_type = car_type
        self.brand = brand
        valid_extensions = [".jpeg", ".jpg", ".png", ".gif"]
        extension = os.path.splitext(photo_file_name)[1]
        if extension not in valid_extensions:
            raise Exception("Not valid extension: {}".format(extension))
        self.photo_file_name = photo_file_name
        self.carrying = carrying

    def get_photo_file_ext(self):
        return os.path.splitext(self.photo_file_name)[1]


class Car(CarBase):
    def __init__(self, brand, photo_file_name, carrying, passenger_seats_count):
        super().__init__(car_type="car", brand=brand, photo_file_name=photo_file_name, carrying=carrying)
        self.passenger_seats_count = passenger_seats_count


class Truck(CarBase):
    def __init__(self, brand, photo_file_name, carrying, body_whl):
        super().__init__(car_type="truck", brand=brand, photo_file_name=photo_file_name, carrying=carrying)
        pattern = r"([0-9.]+x[0-9.]+x[0-9.]+)"
        if not re.match(pattern, body_whl):
            body_whl = "0x0x0"
        self.body_whl = body_whl
        self.body_length, self.body_width, self.body_height = (float(x) for x in self.body_whl.split("x"))

    def get_body_volume(self):
        return self.body_length*self.body_width*self.body_height


class SpecMachine(CarBase):
    def __init__(self, brand, photo_file_name, carrying, extra):
        super().__init__(car_type="spec_machine", brand=brand, photo_file_name=photo_file_name, carrying=carrying)
        self.extra = extra


def from_csv_to_class(csv_row):
    try:
        car_type = csv_row[0]
        brand = csv_row[1]
        photo_file_name = csv_row[3]
        carrying = csv_row[5]
        if car_type == "car":
            passenger_seats_count = csv_row[2]
            car = Car(brand, photo_file_name, carrying, passenger_seats_count)
            return car
        elif car_type == "truck":
            body_whl = csv_row[4]
            truck = Truck(brand, photo_file_name, carrying, body_whl)
            return truck
        elif car_type == "spec_machine":
            extra = csv_row[6]
            spec_machine = SpecMachine(brand, photo_file_name, carrying, extra)
            return spec_machine
        else:
            raise Exception("Not valid car type: {}".format(car_type))
    except Exception:
        pass

def get_car_list():
    cars = []
    with open("/home/o6a-ha/Downloads/coursera_cars.csv") as csv_fd:
        reader = csv.reader(csv_fd, delimiter=';')
        next(reader)
        for row in reader:
            car = from_csv_to_class(row)
            if car is not None:
                cars.append(car)
        return cars

if __name__ == "__main__":
    get_car_list()
