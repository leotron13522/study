import json
import functools


def to_json(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        encode_func = json.dumps(func(*args, **kwargs))
        return encode_func
    return wrapper

