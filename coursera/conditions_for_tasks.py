"""
Это вспомогательный скрипт для тестирования сервера из задания на неделе 6.

Для запуска скрипта на локальном компьютере разместите рядом файл client.py,
где содержится код клиента, который открывается по прохождении задания
недели 5.

Сначала запускаете ваш сервер на адресе 127.0.0.1 и порту 8888, а затем
запускаете этот скрипт.
"""
import sys
#from client import Client, ClientError
import time
import socket


class Client:

    def connection(self, host, port, timeout):
        print(host, port)
        con = socket.socket()
        con.settimeout(timeout)
        con.connect((host, port))
        #con.send(b"trying to connect")
        #print(con, type(con))
        return con

    def __init__(self, host, port, timeout=None):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.connect = self.connection(self.host, self.port, self.timeout)
        #print(type(self.connect))

    def get(self, metrik_name):
        try:
            self.connect.send('get {}\n'.format(metrik_name).encode('utf-8'))
            ans = ""
            while True:
                rec = self.connect.recv(1024)
                print(rec.decode('utf-8'))
                ans = ans + rec.decode('utf-8')
                #ans = ans.decode('utf-8')
                print("incoming message: ", ans)
                if ans[-2:] == '\n\n':
                    if ans[0:2] == "ok":
                        print("get catched")
                        returning_answer = self.making_answer_dict(ans)
                        return returning_answer
                    else:
                        raise ClientError
        except Exception:
            raise ClientError

    def put(self, metrik_name, metrik_value, timestamp=int(time.time())):
        try:
            #print("trying to send")
            self.connect.send('put {} {} {}\n'.format(metrik_name, metrik_value, timestamp).encode('utf-8'))
            print(self.connect.recv(1024))
            #print("success")
        except Exception:
            raise ClientError

    def close_connect(self):
        print("trying to close connection")
        self.connect.close()
        print("con close")

    def making_answer_dict(self, ans):
        print("working with ans")
        ans_work = ans.split("\n")
        print(ans_work)
        ans_dict = {}
        for part in ans_work[1:]:
            if len(part) != 0:
                key, timestamp, value = part.split()
                print("need to include in dict: ", key, value, timestamp)
                if key not in ans_dict:
                    ans_dict[key] = [(int(timestamp), float(value))]
                else:
                    ans_dict[key] += [(int(timestamp), float(value))]
        #ans_dict.sort()
        print(ans_dict)
        return ans_dict


class ClientError(Exception):
    pass

def run(host, port):
    client1 = Client(host, port, timeout=5)
    client2 = Client(host, port, timeout=5)
    command = "wrong command test\n"

    try:
        print("task 1, command is: ", command)
        data = client1.get(command)

    except ClientError:
        pass
    except BaseException as err:
        print(f"Ошибка соединения с сервером: {err.__class__}: {err}")
        sys.exit(1)
    else:
        print("Неверная команда, отправленная серверу, должна возвращать ошибку протокола")
        sys.exit(1)

    command = 'some_key'
    try:
        print("task 2, command is: ", command)
        data_1 = client1.get(command)
        data_2 = client1.get(command)
        print("на сравнение:", data_1, data_2)
    except ClientError:
        print('Сервер вернул ответ на валидный запрос, который клиент определил, '
              'как не корректный.. ')
    except BaseException as err:
        print(f"Сервер должен поддерживать соединение с клиентом между запросами, "
              f"повторный запрос к серверу завершился ошибкой: {err.__class__}: {err}")
        sys.exit(1)

    assert data_1 == data_2 == {}, \
        "На запрос клиента на получения данных по не существующему ключу, сервер " \
        "вдолжен озвращать ответ с пустым полем данных."
    print("сравнение успешно")
    try:
        print("task 3, comand is: ", command)
        data_1 = client1.get(command)
        data_2 = client2.get(command)
    except ClientError:
        print('Сервер вернул ответ на валидный запрос, который клиент определил'
              ', как не корректный.. ')
    except BaseException as err:
        print(f"Сервер должен поддерживать соединение с несколькими клиентами: "
              f"{err.__class__}: {err}")
        sys.exit(1)

    assert data_1 == data_2 == {}, \
        "На запрос клиента на получения данных по не существующему ключу, сервер " \
        "должен возвращать ответ с пустым полем данных."

    try:
        print("task 4, comand is: ", command)
        client1.put("k1", 0.25, timestamp=1)
        client2.put("k1", 2.156, timestamp=2)
        client1.put("k1", 0.35, timestamp=3)
        client2.put("k2", 30, timestamp=4)
        client1.put("k2", 40, timestamp=5)
        client1.put("k2", 41, timestamp=5)
        client1.put("k2", 212, timestamp=3)
    except Exception as err:
        print(f"Ошибка вызова client.put(...) {err.__class__}: {err}")
        sys.exit(1)

    expected_metrics = {
        "k1": [(1, 0.25), (2, 2.156), (3, 0.35)],
        "k2": [(3, 212), (4, 30.0), (5, 41.0)],
    }

    try:
        print("task 5, comand is: ", command)
        metrics = client1.get("*")
        if metrics != expected_metrics:
            print(f"client.get('*') вернул неверный результат. Ожидается: "
                  f"{expected_metrics}. Получено: {metrics}")
            sys.exit(1)
    except Exception as err:
        print(f"Ошибка вызова client.get('*') {err.__class__}: {err}")
        sys.exit(1)

    expected_metrics = {"k2": [(3, 212), (4, 30.0), (5, 41.0)]}

    try:
        print("task 6, comand is: ", command)
        metrics = client2.get("k2")
        if metrics != expected_metrics:
            print(f"client.get('k2') вернул неверный результат. Ожидается: "
                  f"{expected_metrics}. Получено: {metrics}")
            sys.exit(1)
    except Exception as err:
        print(f"Ошибка вызова client.get('k2') {err.__class__}: {err}")
        sys.exit(1)

    try:
        print("task 7, comand is: ", command)
        result = client1.get("k3")
        if result != {}:
            print(
                f"Ошибка вызова метода get с ключом, который еще не был добавлен. "
                f"Ожидается: пустой словарь. Получено: {result}")
            sys.exit(1)
    except Exception as err:
        print(f"Ошибка вызова метода get с ключом, который еще не был добавлен: "
              f"{err.__class__} {err}")
        sys.exit(1)

    print("Похоже, что все верно! Попробуйте отправить решение на проверку.")


if __name__ == "__main__":
    run("127.0.0.1", 8888)

"""
import asyncio


class ClientServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        resp = process_data(data.decode())
        self.transport.write(resp.encode())


loop = asyncio.get_event_loop()
coro = loop.create_server(
    ClientServerProtocol,
    '127.0.0.1', 8181
)

server = loop.run_until_complete(coro)

try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
"""
"""
$: telnet 127.0.0.1 8888
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
> get test_key
< ok
< 
> got test_key
< error
< wrong command
< 
> put test_key 12.0 1503319740
< ok
< 
> put test_key 13.0 1503319739
< ok
< 
> get test_key 
< ok
< test_key 13.0 1503319739
< test_key 12.0 1503319740
< 
> put another_key 10 1503319739
< ok
< 
> get *
< ok
< test_key 13.0 1503319739
< test_key 12.0 1503319740
< another_key 10.0 1503319739
"""