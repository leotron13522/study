import argparse
import asyncio
import aiohttp
import time
import requests
from concurrent.futures import ThreadPoolExecutor


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', dest='url')
    parser.add_argument('--count', dest='count', type=int)
    parser.add_argument('--workers', dest='workers', type=int)
    parser.add_argument('--async', dest='async', action='store_true')

    args = parser.parse_args()

    begin = time.time()
    if args.async:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(fetch_async(args.url, args.count))
    else:
        fetch_sync(args.url, args.count, args.workers)
    dur = time.time() - begin
    print(f"Done {args.count} requests in {dur:.3f} sec, rps: {(args.count / dur): .3f}")


def fetch_sync(url, count, workers):
    with ThreadPoolExecutor(max_workers=workers) as pool:
        pool.map(lambda: requests.get(url), range(count))


async def fetch_async(url, count):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            body = await response.text()


if __name__ == "__main__":
    main()



