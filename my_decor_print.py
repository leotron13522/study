import sys

def my_decor_print(func):
    def wrapper(*args, **kwargs):
        sys.stdout.write("+"*10+"\n")
        func(*args, **kwargs)
        sys.stdout.write("-"*10+"\n")
    return wrapper

print = my_decor_print(print)
print()