﻿# include <iostream>
# include <locale>
#include <math.h>
#define _USE_MATH_DEFINES
//#include <curses.h>
#include <stdio.h>
#include <vector>
#include <ctype.h>
#include <string.h>
#include <Windows.h>

using namespace std;


void task1() {
	char str[80];
	char result[180];
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	memset(result, 0, sizeof(result));
	cout << "введите строку  ";
	gets_s(str, 80);
	cout << "введенная строка: " << endl;
	cout << str << endl;
	puts(str);
	cout << endl;
	int count_eng = 0, count_rus = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] >= 'a' && str[i] <= 'z' || str[i] >= 'A' && str[i] <= 'Z') {
			count_eng++;
		}
		else if (str[i] >= 'а' && str[i] <= 'я' || str[i] >= 'А' && str[i] <= 'Я') {
			count_rus++;
			//cout << count_rus;
		}
	}
	if (count_eng < count_rus)
	{
		for (int i = 0; i < strlen(str); i++)
		{
			if (str[i] >= 'а' && str[i] <= 'я' || str[i] >= 'А' && str[i] <= 'Я' || isalpha(str[i])) {
				result[i] = toupper(str[i]);
			}
			else {
				result[i] = str[i];
			}
		}
	}
	else {
		int j = 0;
		for (int i = 0; i < strlen(str); i++) {
			if (str[i] < '0' || str[i] > '9') {
				result[j++] = str[i];
			}
		}
		result[j] = '\0';

	}
	cout << "Result is: ";
	puts(result);
}

void task2() {
	FILE *f1;
	cout << "Введите имя файла " << endl;
	char file_name[80];
	gets_s(file_name);
	fopen_s(&f1, file_name, "w");
	cout << "Введите строки в файл " << endl;
	char str[80];
	int i = 1;
	char str_numb[180];
	do {
		gets_s(str);
		if (strlen(str) == 0) {
			break;
		}
		fprintf_s(f1, "%d %s \n", i, str);
		i++;
	} while (str[0]);
	fclose(f1);
	FILE *f2;
	cout << "Ввывод инвормации из файла " << endl;
	fopen_s(&f2, file_name, "r");
	while (fgets(str, 80, f2)) {
		fputs(str, stdout);
	}
	fclose(f2);

}

char solution1() {
	char str[80];
	cout << "Введите строку" << endl;
	cin.ignore();
	gets_s(str, 80);
	//puts(str);
	cout << "Введенная строка: " << str << endl;
	return str[80];
}

void task3() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int tn;
	do
	{
	cout << "Выберете действие: " << endl;
	cout << "1. Ввод строки с клавиатуры" << endl;
	cout << "2. Вывод строки на экран" << endl;
	cout << "3. Зашифровать строку" << endl;
	cout << "4. Расшифровать строку" << endl;
	cout << "5. Записать строку в файл" << endl;
	cout << "6. Считать строку из файла" << endl;
	cout << "0. Выход" << endl;
	cin >> tn;
	char in_str[80];
	switch (tn) {
		case 1: 
			cout << "Введите строку" << endl;
			cin.ignore();
			gets_s(in_str);
			break;
		case 2:
			cout << "Строка: " << endl;
			puts(in_str);
			break;
		/*
		case 3: for3();
		break;
		case 4: for4();
		break;
		case 5: for5();
		break;
		case 6: for6();
		break;
		*/
		}
	} while (tn != 0);

}



int main() {
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	//task1();
	//task2();
	task3();

	system("Pause");
}
