# include <iostream>
# include <locale>
#include <math.h>
#define _USE_MATH_DEFINES
#include <curses.h>
//#include <stdio.h>

using namespace std;

void task1() {
	cout << "Задача 1" << endl;
    cout << "Введите три числа" << endl;
    double a, b, c, f;

    scanf("%lf%lf%lf", &a, &b, &c);
    printf("--------------------------------\n");

    printf("|       X     |        F      |\n");

    printf("--------------------------------\n");
    double x = -1;
    while (x <= 1){
        if (x+5 < 0 && c == 0){
            if (a*x != 0){
                f = 1/(a*x) - b;
            }
            else {
                f = 0;
                //cout << "Деление на ноль" << endl;
            }
        }
        else if (x+5 > 0 && c != 0){
            if (x != 0){
                f = (x-a) / x;
            }
            else{
                f = 0;
                //cout << "Деление на ноль" << endl;
            }
        }
        else {
            if (c-4 != 0){
                f = (10*x) / (c-4);
            }
            else{
                f = 0;
                //cout << "Деление на ноль" << endl;
            }
        }
        printf("|% 9.2f    |% 9.2f      |\n", x, f);
        x += 0.05;
    }

    printf("--------------------------------\n");

}

void task2() {
	cout << "Задача 2" << endl;
    cout << "Введите числo" << endl;
    int x;
    cin >> x;
    for (int i = 1; i <= x; i++){
        cout << x << " " << i;
        for (int k = (x-i); k != 0; k--){
            cout << " ";
        }
        for (int j = 0; j < i; j++){
            cout << "*";
        }
        cout << endl;
    }

}

void task3() {
	cout << "Задача 3" << endl;
	cout << "Введите число х" << endl;
    double a = 0.1, b = 1.0, h = 0.1, n = 20, x, S=0, Y;
    cin >> x;
    printf("---------------------------------------------------------------\n");

    printf("|     x       |      S(x)     |     Y(x)      |  |Y(x)-S(x)|  |\n");
    while (a <= b) {
        printf("---------------------------------------------------------------\n");
        for (int k = 0; k <= n; k++){
            double fact = 1;
            for (int i = 1; i <= 2*k; i++) {
                fact *= i;
            }
            S += pow(-1,k)*(pow(x,(2*k))/fact);
        }
        Y = cos(x);
        printf("|% 9.2f    |% 9.5f      |% 9.5f      |% 9.5f      |\n", x, S, Y, abs(Y-S));
        a += h;
    }
    printf("---------------------------------------------------------------\n");

}



int main() {
	setlocale(LC_ALL, "Russian");

	int tn;
	do
	{
		cout << "Ведите номер задания (1-3) 0 - для выхода:" << endl;
		cin >> tn;
		switch (tn) {
		case 1: task1();
			break;
		case 2: task2();
			break;
		case 3: task3();
			break;
		}
	} while (tn != 0);


}
