def who_eats_who(input):
    animal = input.split(',')
    ans = []
    ans.append(input)
    eaten_dict = {
        "antelope": "grass",
        "big-fish": "little-fish",
        "bug": "leaves",
        "bear": ["big-fish", "bug", "chicken", "cow", "leaves", "sheep"],
        "chicken": "bug",
        "cow": "grass",
        "fox": ["chicken", "sheep"],
        "giraffe": "leaves",
        "lion": ["antelope", "cow"],
        "panda": "leaves",
        "sheep": "grass"
    }
    def eaten(animal, eaten_dict, ans):
        for x in range(len(animal)):
            print(len(animal))
            if animal[x] in eaten_dict:
                print(animal[x])
                if x-1 >= 0 and animal[x-1] in eaten_dict[animal[x]]:
                    ind = x-1
                elif x+1 < len(animal) and animal[x+1] in eaten_dict[animal[x]]:
                    ind = x+1
                else:
                    continue
                ans.append(animal[x] + " eats " + animal[ind])
                print(animal[x] + " eats " + animal[ind])
                animal.pop(ind)
                return eaten(animal, eaten_dict, ans)
        ans_2 = ','.join(str(i) for i in animal)
        ans.append(ans_2)
    eaten(animal, eaten_dict, ans)
    return ans

input = "fox,bug,chicken,grass,sheep"
print((who_eats_who(input)))
